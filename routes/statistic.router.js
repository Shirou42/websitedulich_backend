const express = require('express');
const router = express.Router();
const statisticController = require('../controllers/statistic.controller');

// thống kê 2 này trước
// order các tháng trong năm
router.post('/api/statistic/order/month-year', statisticController.getOrderByMonthInYear);
// order qua các năm
router.post('/api/statistic/order/year', statisticController.getOrderByYear);

// Sản phẩm trong 1 tháng
router.post('/api/statistic/product/month', statisticController.getNumberProductInMonth);

// tour trong 1 tháng
router.post('/api/statistic/tour/month', statisticController.getNumberTourInMonth);

// doanh thu theo month
router.post('/api/statistic/revenue/month', statisticController.getRevenueInMonth);

// doanh thu theo quarter
router.post('/api/statistic/revenue/quarter', statisticController.getRevenueInQuarter);

router.post('/api/statistic/order/month', statisticController.getOrderByMonth);


module.exports = router;
