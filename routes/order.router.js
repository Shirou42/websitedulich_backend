const express = require('express');
const router = express.Router();
const orderController = require('../controllers/order.controller');

// api place
router.get('/api/orders', orderController.getAllOrder);
router.post('/api/orders/detail', orderController.getOrderDetal);
router.get('/api/orders/fail', orderController.getOrderFail);
router.get('/api/orders/success', orderController.getOrderSuccess);
router.get('/api/orders/tours', orderController.getTourByOrder);
router.put('/api/cancelorder/:id', orderController.cancelOrder);
router.put('/api/checkorder/:id', orderController.checkOrder);
router.post('/api/getcustomerstour', orderController.getListCustomerTour)

module.exports = router;
