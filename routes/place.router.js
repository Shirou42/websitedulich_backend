const express = require('express');
const router = express.Router();
const placeController = require('../controllers/place.controller');

// api place
router.get('/api/places', placeController.getAllPlace);
router.post('/api/get-place', placeController.getPlaceById);
router.post('/api/place', placeController.addPlace);
router.put('/api/place/:id', placeController.updatePlace);
router.delete('/api/place/:id', placeController.deletePlace);

module.exports = router;
