const express = require('express');
const router = express.Router();
const employeeController = require('../controllers/employee.controller');

// api employee
router.get('/api/employees', employeeController.getAllEmployee);
router.post('/api/get-employee', employeeController.getEmployeeById);
router.post('/api/employee', employeeController.addEmployee);
router.put('/api/employee/:id', employeeController.updateEmployee);
router.delete('/api/employee/:id', employeeController.deleteEmployee);

module.exports = router;
