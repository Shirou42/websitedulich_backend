const express = require('express');
const router = express.Router();
const hotelController = require('../controllers/hotel.controller');

router.get('/api/hotels', hotelController.getAllHotel);
router.post('/api/get-hotel', hotelController.getHotelById);
router.post('/api/hotel', hotelController.addHotel);
router.put('/api/hotel/:id', hotelController.updateHotel);
router.delete('/api/hotel/:id', hotelController.deleteHotel);
module.exports = router;
