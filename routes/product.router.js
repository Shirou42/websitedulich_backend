const express = require('express');
const router = express.Router();
const productController = require('../controllers/product.controller');

// api product
router.get('/api/products', productController.getAllProduct);
router.post('/api/get-product', productController.getProductById);

router.post('/api/product', productController.addProduct);
router.put('/api/product/:id', productController.updateProduct);
router.delete('/api/product/:id', productController.deleteProduct);

module.exports = router;
