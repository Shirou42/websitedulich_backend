const express = require('express')
const router = express.Router()
const crawController = require('../controllers/crawler-controller')

router.get('/tours',crawController.crawlTour);
router.get('/products',crawController.crawlProduct);

module.exports= router; 