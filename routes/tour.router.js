const express = require('express');
const router = express.Router();
const tourController = require('../controllers/tour.controller');

// api tour
router.get('/api/tours', tourController.getAllTour);
router.post('/api/get-tour', tourController.getTourById);
router.post('/api/get-tour-update', tourController.getTourByIdUpdate);
router.post('/api/tour', tourController.addTour);
router.put('/api/tour/:id', tourController.updateTour);
router.delete('/api/tour/:id', tourController.deleteTour);

router.get('/api/tours/status/true', tourController.getTourStatusTrue);
router.get('/api/tours/status/false', tourController.getTourStatusFail);
module.exports = router;
