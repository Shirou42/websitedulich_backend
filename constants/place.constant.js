//Thêm mới địa điểm
module.exports.ADD_PLACE_FAIL="Thêm PLACE thất bại"
module.exports.ADD_PLACE_SUCCESS="Thêm PLACE thành công"

// cập nhật một địa điểm
module.exports.UPDATE_PLACE_FAIL="Update PLACE thất bại"
module.exports.UPDATE_PLACE_SUCCESS="Update PLACE thành công"

// xóa một địa điểm
module.exports.DELETE_PLACE_FAIL="Xóa PLACE thất bại"
module.exports.DELETE_PLACE_SUCCESS="Xóa PLACE thành công"

// tìm kiếm địa điểm
module.exports.FOUND_PLACE_SUCCESS="tìm thấy PLACE"
module.exports.NOT_FOUND_PLACE="Không tìm thấy PLACE"

module.exports.PLACE_EXIST = "PLACE đã tồn tại"

// Lỗi hệ thống
module.exports.SYSTEM_ERROR = "Lỗi hệ thống"