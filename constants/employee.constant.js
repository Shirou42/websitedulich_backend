//Thêm mới nhân viên
module.exports.ADD_EMPLOYEE_FAIL="Thêm nhân viên thất bại"
module.exports.ADD_EMPLOYEE_SUCCESS="Thêm nhân viên thành công"

// cập nhật một nhân viên
module.exports.UPDATE_EMPLOYEE_FAIL="Update nhân viên thất bại"
module.exports.UPDATE_EMPLOYEE_SUCCESS="Update nhân viên thành công"

// xóa một nhân viên
module.exports.DELETE_EMPLOYEE_FAIL="Xóa nhân viên thất bại"
module.exports.DELETE_EMPLOYEE_SUCCESS="Xóa nhân viên thành công"

// tìm kiếm nhân viên
module.exports.FOUND_EMPLOYEE_SUCCESS="Tìm thấy nhân viên"
module.exports.NOT_FOUND_EMPLOYEE="Không tìm thấy nhân viên"
module.exports.EMPLOYEE_EXIST = "Nhân viên đã tồn tại"

// Lỗi hệ thống
module.exports.SYSTEM_ERROR = "Lỗi hệ thống"

