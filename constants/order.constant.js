// tìm hóa đơn
module.exports.NOT_FIND_ORDER="Không tìm thấy hóa đơn"
module.exports.FIND_ORDER_FAIL="Tìm hóa đơn thất bại"
module.exports.FIND_ORDER_SUCCESS="Tìm hóa đơn thành công"


//Tạo order

module.exports.CREATE_ORDER_SUCCESS="Tạo hóa đơn thành công"
module.exports.CREATE_ORDER_FAIL="Tạo hóa đơn thất bại"

// xóa order
module.exports.DELETE_ORDER_FAIL="Xóa hóa đơn thất bại"

//Check hóa đơn
module.exports.CHECK_ORDER_FAIL="Check hóa đơn thất bại"

// Lỗi hệ thống
module.exports.SYSTEM_ERROR = "Lỗi hệ thống"



