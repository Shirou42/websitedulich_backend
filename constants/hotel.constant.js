//Thêm mới khách sạn
module.exports.ADD_HOTEL_FAIL="Thêm HOTEL thất bại"
module.exports.ADD_HOTEL_SUCCESS="Thêm HOTEL thành công"

// cập nhật một khách sạn
module.exports.UPDATE_HOTEL_FAIL="Update HOTEL thất bại"
module.exports.UPDATE_HOTEL_SUCCESS="Update HOTEL thành công"

// xóa một khách sạn
module.exports.DELETE_HOTEL_FAIL="Xóa HOTEL thất bại"
module.exports.DELETE_HOTEL_SUCCESS="Xóa HOTEL thành công"

// tìm kiếm khách sạn
module.exports.FOUND_HOTEL_SUCCESS="tìm thấy HOTEL"
module.exports.NOT_FOUND_HOTEL="Không tìm thấy HOTEL"

module.exports.HOTEL_EXIST = "HOTEL đã tồn tại"

// Lỗi hệ thống
module.exports.SYSTEM_ERROR = "Lỗi hệ thống"

