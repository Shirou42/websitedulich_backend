const puppeteer = require('puppeteer')
const tour = require("../models/tour.model");
const product = require("../models/product.model");
// const hotel = require('../models/hotel.model')
const place = require('../models/place.model')
var moment = require('moment');
const employee = require("../models/employee.model");
const imageDBS3 = require("../config/imageDBS3");

const crawlTour = async (req, res) => {
  console.log('Start')
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      '--disable-notifications',
      '--unlimited-storage',
      '--full-memory-crash-report',
      '--disable-gpu',
      '--ignore-certificate-errors',
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
      '--lang=en-US;q=0.9,en;q=0.8',
      '--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process'
    ]
  })
  const page = await browser.newPage();
  page.setViewport({ width: 1280, height: 720 });
  await page.goto('https://travel.com.vn/du-lich-nha-trang.aspx', { waitUntil: 'networkidle2', timeout: 0 })
  const ListRealestate = await page.evaluate(async () => {
    const TOUR_LIST = document.querySelectorAll(
      '.list > .row > div >.col-md-11.col-sm-12.col-xs-12 > .item > .row '
    )
    const list = [];
    TOUR_LIST.forEach(async (tourItem) => {
      // get
      const title = tourItem.querySelector('.col-xs-12 > .tour-name').innerText
      const href = tourItem.querySelector('.col-xs-12 > .tour-name > a').href
      const image = tourItem.querySelector('.col-lg-3.col-md-3.col-sm-3.col-xs-12 > .pos-relative > a > img').src
      const rate = tourItem.querySelectorAll('.col-lg-9.col-md-9.col-sm-9.col-xs-12> .frame-info> .row > .mg-bot10').item(0).querySelector('.group-listtour > .rateit').dataset.rateitValue
      const place = tourItem.querySelectorAll('.col-lg-9.col-md-9.col-sm-9.col-xs-12>.frame-info> .row > .mg-bot10').item(6).querySelector('.r>span').innerText
      const quantiDate = tourItem.querySelectorAll('.col-lg-9.col-md-9.col-sm-9.col-xs-12 >.frame-info > .row > .col-md-5').item(2).querySelector('.r>span').innerText
      const numberTicket = tourItem.querySelectorAll('.col-lg-9.col-md-9.col-sm-9.col-xs-12 >.frame-info > .row > .col-md-5').item(0).querySelector('.r>span').innerText
      // set
      const realestate = {
        title: title,
        href: href,
        image: image,
        rate: rate,
        quantiDate: quantiDate,
        numberTicket: numberTicket,
        place: place
      }
      list.push(realestate);
    })
    return list;
  })
  for (let index = 0; index < ListRealestate.length; index++) {
    const element = ListRealestate[index];
    try {

      await page.goto(ListRealestate[index].href, {
        waitUntil: 'load',
        timeout: 0
      })
      const detail = await page.waitForSelector('#theogia > .mg-bot30').then(async () => {
        const detailObj = await page.evaluate(async () => {
          const tourcode = document.querySelector('.tour-code').innerText
          const LISTIMAGE = document.querySelectorAll('.carousel-inner > .item')
          const imageTour = [];
          const imageName = [];
          for (let index = 0; index < LISTIMAGE.length; index++) {
            const element = LISTIMAGE[index];
            const image = element.querySelector('img').src;
            var splitImage = image.split(/[/]+/)
            imageName.push(splitImage[splitImage.length-1]);
            imageTour.push(image);
          }
          const DetailTour = document.querySelectorAll('.list >.list__item')
          const ListDetail = [];
          const ListDay = [];
          const ListNum = [];
          const ListTitle = [];
          DetailTour.forEach(async (detail) => {
            const num = detail.querySelector('.list__time > .num').innerText
            const day = detail.querySelector('.list__time > .day').innerText
            const titleDay = detail.querySelector('.list__desc > .name').innerText
            const detailDay = '<div class="d1 detail">' + detail.querySelector('.list__desc > .detail').innerHTML + '</div>';
            ListTitle.push(titleDay)
            ListDetail.push(detailDay)
            ListDay.push(day)
            ListNum.push(num)
          })
          document.querySelector('.tab-luuy').click()
          document.querySelector('.tab-chitiettour').click()
          const pricepeple = document.querySelectorAll('.tour-price > .row > .col-xs-12 > .table-bordered > tbody > tr')
            .item(0).querySelectorAll('td').item(1).innerText
          const price12 = document.querySelectorAll('.tour-price > .row > .col-xs-12 > .table-bordered > tbody > tr')
            .item(1).querySelectorAll('td').item(1).innerText
          const price5 = document.querySelectorAll('.tour-price > .row > .col-xs-12 > .table-bordered > tbody > tr')
            .item(2).querySelectorAll('td').item(1).innerText
          const surcharge = document.querySelectorAll('.tour-price > .row > .col-xs-12 > .table-bordered > tbody > tr')
            .item(4).querySelectorAll('td').item(1).innerText
          return {
            tourcode: tourcode, imageTour: imageTour,imageName:imageName, ListDetail: ListDetail, ListDay: ListDay, ListNum: ListNum, ListTitle: ListTitle,
            pricepeple: pricepeple, price5: price5, price12: price12, surcharge: surcharge
          };
        })
        return detailObj;
      })
      ListRealestate[index] = { element, detail };
      const employeeid = await employee.findOne({})
      ListTransportation = [];
      for (let index = 0; index < detail.ListDay.length; index++) {
        ListTransportation.push("Máy bay")
      }

      const ListPlace = [];
      var placeSplit = element.title.split(/[-–]+/)
      for (let index = 1; index < placeSplit.length - 1; index++) {
        var placeExist = await place.findOne({ placeName: placeSplit[index].trim()})
        if (placeExist == null) {
          var newPlace = new place({
            placeName: placeSplit[index].trim(),
            type: "Tham quan",
            city: placeSplit[index].trim(),
            district: placeSplit[index].trim(),
            apartmentNumber: placeSplit[index].trim(),
          })
          newPlace.save(function (err) {
            if (err)
              console.log(err);
          });
          ListPlace.push(newPlace._id);
        } else {
          ListPlace.push(placeExist._id)
        }
      }

      var firstPlace = placeSplit[0].split(/[:]+/)
      var firstPlaceEnd=null;
      if(firstPlace.length > 1){
        firstPlaceEnd =  firstPlace[1]
      }else{
        firstPlaceEnd =  firstPlace[0]
      }
      var placeExist = await place.findOne({ placeName: firstPlaceEnd.trim() })
      if (placeExist == null) {
        var newPlace = new place({
          placeName: firstPlaceEnd.trim(),
          type: "Tham quan",
          city: firstPlaceEnd.trim(),
          district: firstPlaceEnd.trim(),
          apartmentNumber: firstPlaceEnd.trim(),
        })
        newPlace.save(function (err) {
          if (err)
            console.log(err);
        });
        ListPlace.push(newPlace._id);
      } else {
        ListPlace.push(placeExist._id)
      }

      var lastPlaceEnd = placeSplit[placeSplit.length-1].split(/[()]+/)[0]
      var placeExist = await place.findOne({ placeName: lastPlaceEnd.trim() })
      if (placeExist == null) {
        var newPlace = new place({
          placeName: lastPlaceEnd.trim(),
          type: "Tham quan",
          city: lastPlaceEnd.trim(),
          district: lastPlaceEnd.trim(),
          apartmentNumber: lastPlaceEnd.trim(),
        })
        newPlace.save(function (err) {
          if (err)
            console.log(err);
        });
        ListPlace.push(newPlace._id);
      } else {
        ListPlace.push(placeExist._id)
      }
      const uploadS3 =[];
      for (let index = 0; index < detail.imageTour.length; index++) {
        var uploadImage = await imageDBS3.downloadAttachment(detail.imageTour[index],detail.imageName[index])
        uploadS3.push(uploadImage);
      }
      console.log(uploadS3);
      var newtour = new tour({
        tourId: detail.tourcode,
        tourName: element.title,
        startDate:moment(detail.ListDay[0],"DD-MM-YYYY"),
        endDate: moment(detail.ListDay[detail.ListDay.length - 1],"DD-MM-YYYY"),
        numberOfDays: element.quantiDate,
        numberOfParticipants: 40,
        priceDetail: {
          adult: catChuoiPrice(detail.pricepeple),
          underTheAgeOfTwelve: catChuoiPrice(detail.price12),
          underTheAgeOfFive: catChuoiPrice(detail.price5),
        },
        typesOftourism: "Tham quan",
        employee: employeeid._id,
        startPlace: element.place,
        listImage: uploadS3,
        place: ListPlace,
        hotel: null,
        transportation: ListTransportation,
        detail: detail.ListDetail,
        numDay: detail.ListNum,
        rate: element.rate,
        surcharge: catChuoiPrice(detail.surcharge),
        numberTicket: element.numberTicket
      });
      console.log(newtour);
      console.log(ListPlace);
      newtour.save(function (err) {
        if (err)
          console.log(err);
      });
    } catch (error) {
      console.log(error)
      await page.reload(element.href)
    }
  }

  console.log("End")
  await browser.close()
}

const crawlProduct = async (req, res) => {
  console.log('Start')
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      '--disable-notifications',
      '--unlimited-storage',
      '--full-memory-crash-report',
      '--disable-gpu',
      '--ignore-certificate-errors',
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
      '--lang=en-US;q=0.9,en;q=0.8',
      '--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process'
    ]
  })
  const page = await browser.newPage();
  page.setViewport({ width: 1280, height: 720 });
  await page.goto('https://umove.com.vn/binh-nuoc-loc-nuoc-pc81.html', { waitUntil: 'networkidle2', timeout: 0 })
  const ListRealestate = await page.evaluate(async () => {
    const PRODUCT_LIST = document.querySelectorAll('.item-package-product')
    const list = [];
    PRODUCT_LIST.forEach(async (productItem) => {
      // get

      const href = productItem.querySelector('.title-item-package > h4 > a').href
      const name = productItem.querySelector('.title-item-package > h4 > a').innerText
      const price = productItem.querySelector('.title-item-package >p').innerText
      const priceProduct = price.split(/[\s,.,VNĐ]/);
      let priceConvert = "";
      for (let index = 0; index < priceProduct.length; index++) {
        const element = priceProduct[index];
        priceConvert = priceConvert + element;
      }

      // set
      const realestate = {
        href: href,
        name: name,
        price: priceConvert
      }
      list.push(realestate);
    })
    return list;
  })
  // console.log(ListRealestate[0]);
  // const imageUpload = ListRealestate[0].image.split('/')
  // let uploadS3 = await imageDBS3.downloadAttachment(ListRealestate[0].image,imageUpload[imageUpload.length-1]);
  // console.log(uploadS3);
  for (let index = 0; index < ListRealestate.length ; index++) {
    const element = ListRealestate[index];
    try {

      await page.goto(ListRealestate[index].href, {
        waitUntil: 'load',
        timeout: 0
      })
      const detail = await page.waitForSelector('.owl-item').then(async () => {
        const detailObj = await page.evaluate(async () => {
          const productCode = document.querySelector('.sku').innerText
          const description = document.querySelector('.ctn-left-detail').innerHTML
          const image = document.querySelector('.xzoom-container > img').src
          // const typesOftourism = document.querySelectorAll('td').item(3).innerText
          // const ListTypesOftourism = typesOftourism.split(',');
          var splitImage = image.split(/[/]+/)
          return {
            productCode: productCode, description: description,image:image,imageName:splitImage[splitImage.length-1]
          };
        })
        return detailObj;
      })
      var uploadImage = await imageDBS3.downloadAttachment(detail.image,detail.imageName)
      console.log(uploadImage);
      var newProduct = new product({
        productCode: detail.productCode,
        productName: element.name,
        price: element.price,
        typesOftourism: ["Tham quan", "Du lịch", "Thám hiểm"],
        image: uploadImage,
        quantity: 40,
        description: detail.description
      });
      newProduct.save(function (err) {
        if (err)
          console.log(err);
      });
      console.log(newProduct);
    } catch (error) {
      console.log(error)
      await page.reload(element.href)
    }
  }


  console.log(ListRealestate);
  console.log("End")
  await browser.close()
}
function catChuoiPrice(chuoi) {
  const catchuoi = chuoi.split(/[\s,]+/);
  var hopchuoi = "";
  for (let index = 0; index < catchuoi.length - 1; index++) {
    const element = catchuoi[index];
    hopchuoi = hopchuoi + element;
  }
  return hopchuoi;
}
module.exports = {
  crawlTour,
  crawlProduct
}