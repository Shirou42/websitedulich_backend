const tour = require("../models/tour.model");
const tourService = require("../services/tour.service");
const TIME = require("../utils/time");
const TOURCONSTANT = require("../constants/tour.constant");
const imageDBS3 = require("../config/imageDBS3");
var moment = require("moment");

const getAllTour = async (req, res) => {
  try {
    const allTour = await tour
      .find({})
      .populate(["hotel", "place", "employee"]);
    if (allTour.length != 0) {
      res.status(200).send(allTour);
    } else {
      res.status(200).send(TOURCONSTANT.NOT_FOUND_TOUR);
    }
  } catch (error) {
    res.status(500).send({ message: TOURCONSTANT.SYSTEM_ERROR });
  }
};

const getTourById = async (req, res) => {
  try {
    const id = req.body.id;
    const tourById = await tour
      .findById({ _id: id })
      .populate(["hotel", "place", "employee"]);
    if (tourById.length != 0) {
      res.status(200).send(tourById);
    } else {
      res.status(200).send(TOURCONSTANT.NOT_FOUND_TOUR);
    }
  } catch (error) {
    res.status(500).send({ message: TOURCONSTANT.SYSTEM_ERROR });
  }
};
const getTourByIdUpdate = async (req, res) => {
  try {
    const id = req.body.id;
    const tourById = await tour.findById({ _id: id });
    if (tourById.length != 0) {
      res.status(200).send(tourById);
    } else {
      res.status(200).send(TOURCONSTANT.NOT_FOUND_TOUR);
    }
  } catch (error) {
    res.status(500).send({ message: TOURCONSTANT.SYSTEM_ERROR });
  }
};
const addTour = async (req, res) => {
  try {
    const startDate = req.body.startDate;
    const endDate = req.body.endDate;
    // TÍNH NGÀY ĐI
    const numberOfDays = await TIME.timeDifference(endDate, startDate);
    // ép kiểu date
    var sDate = new Date(moment(startDate));
    sDate.setDate(sDate.getDate() + 1);
    var eDate = new Date(moment(endDate));
    eDate.setDate(eDate.getDate() + 1);

    // TẠO MÃ TOUR RANDOM
    // const tourId = await tourService.randomTourId();
    var uuid = require("uuid");
    const tourId = uuid.v4();

    // XEM IMAGES CÓ NULL HAY KHÔNG -> KHÔNG THÌ THỰC HIỆN THÊM ... CÓ THÌ RA LỖI
    let files = req.files.file;
    const listImage = [];
    if (files != null) {
      // UPLOAD images lên s3 và gắn vào chi tiết tour
      for (let i = 0; i < files.length; i++) {
        let image = await files[i];
        const uploadS3 = await imageDBS3.uploadImages(image);
        listImage.push(uploadS3);
      }

      // Tiều đề ngày
      const numDay = [];
      for (let i = 0; i < numberOfDays; i++) {
        numDay.push("Ngày " + (i + 1));
      }

      if (tourId != -1) {
        const newTour = new tour({
          tourId: tourId,
          tourName: req.body.tourName,
          startDate: sDate,
          endDate: eDate,
          numberOfDays: numberOfDays,
          numberOfParticipants: req.body.numberOfParticipants,
          priceDetail: {
            adult: req.body.adult,
            underTheAgeOfTwelve: req.body.underTheAgeOfTwelve,
            underTheAgeOfFive: req.body.underTheAgeOfFive,
          },
          typesOftourism: req.body.typesOftourism,
          startPlace: req.body.startPlace,
          employee: req.body.employee,
          // thiếu hình
          listImage: listImage,
          place: req.body.place,
          transportation: req.body.transportation,
          hotel: req.body.hotel,
          detail: req.body.detail,
          numDay: numDay,
          // surcharge: req.body.surcharge,
          // số lượng người tham gia bằng số vé
          numberTicket: req.body.numberOfParticipants,
        });
        newTour
          .save()
          .then((value) => {
            res.status(200).send(value);
          })
          .catch((error) => {
            res.status(500).send(TOURCONSTANT.ADD_TOUR_SUCCESS);
          });
      } else {
        res.status(500).send(TOURCONSTANT.CREATE_TOURID_FAIL);
      }
    } else {
      res.status(500).send(TOURCONSTANT.TOUR_IMAGE_FAIL);
    }
  } catch (error) {
    res.status(500).send({ message: TOURCONSTANT.SYSTEM_ERROR });
  }
};

const deleteTour = async (req, res) => {
  try {
    const id = req.params.id;
    tour.findByIdAndDelete({ _id: id }, function (err) {
      if (err) {
        res.status(500).send(TOURCONSTANT.DELETE_TOUR_FAIL);
      } else {
        res.status(200).send(TOURCONSTANT.DELETE_TOUR_SUCCESS);
      }
    });
  } catch (error) {
    res.status(500).send({ message: TOURCONSTANT.SYSTEM_ERROR });
  }
};

const updateTour = async (req, res) => {
  try {
    const id = req.params.id;
    const {
      tourName,
      startDate,
      endDate,
      numberOfParticipants,
      adult,
      underTheAgeOfFive,
      underTheAgeOfTwelve,
      typesOftourism,
      startPlace,
      employee,
      place,
      hotel,
      transportation,
      detail,
      numDay,
      rate,
      numberTicket,
      statusTour,
    } = req.body;
    // const listImage = [];
    // let files = req.files;
    const tourExist = await tour.findById({ _id: id });
    if (tourExist != null || tourExist != []) {
      let query = {};
      tourName ? (query.tourName = tourName) : "";
      if (startDate !== undefined && endDate !== undefined) {
        const numberOfDays = await TIME.timeDifference(endDate, startDate);
        query.numberOfDays = numberOfDays;
        var sDate = new Date(startDate);
        sDate.setDate(sDate.getDate() + 1);
        var eDate = new Date(endDate);
        eDate.setDate(eDate.getDate() + 1);
        query.startDate = sDate;
        query.endDate = eDate;
      }
      let priceDetail = {};
      adult
        ? (priceDetail.adult = adult)
        : (priceDetail.adult = tourExist.priceDetail.adult);
      underTheAgeOfFive
        ? (priceDetail.underTheAgeOfFive = underTheAgeOfFive)
        : (priceDetail.underTheAgeOfFive =
            tourExist.priceDetail.underTheAgeOfFive);
      underTheAgeOfTwelve
        ? (priceDetail.underTheAgeOfTwelve = underTheAgeOfTwelve)
        : (priceDetail.underTheAgeOfTwelve =
            tourExist.priceDetail.underTheAgeOfTwelve);
      query.priceDetail = priceDetail;
      numberOfParticipants
        ? (query.numberOfParticipants = numberOfParticipants)
        : "";
      typesOftourism ? (query.typesOftourism = typesOftourism) : "";
      startPlace ? (query.startPlace = startPlace) : "";
      employee ? (query.employee = employee) : "";
      place ? (query.place = place) : "";
      hotel ? (query.hotel = hotel) : "";
      transportation ? (query.transportation = transportation) : "";
      detail ? (query.detail = detail) : "";
      numDay ? (query.numDay = numDay) : "";
      rate ? (query.rate = rate) : "";
      numberTicket ? (query.numberTicket = numberTicket) : "";
      statusTour ? (query.statusTour = statusTour) : "";
      // if(files == null){
      tour.findByIdAndUpdate({ _id: id }, { $set: query }, function (err) {
        if (err) {
          res.status(500).send(TOURCONSTANT.UPDATE_TOUR_FAIL);
        } else {
          res.status(200).send(TOURCONSTANT.UPDATE_TOUR_SUCCESS);
        }
      });
    } else {
      res.status(500).send(TOURCONSTANT.TOUR_NOT_EXIST);
    }
  } catch (error) {
    res.status(500).send({ message: TOURCONSTANT.SYSTEM_ERROR });
  }
};
const getTourStatusTrue = async (req, res) => {
  try {
    const allTour = await tour
      .find({ statusTour: true })
      .populate(["hotel", "place", "employee"]);
    if (allTour.length != 0) {
      res.status(200).send(allTour);
    } else {
      res.status(200).send(TOURCONSTANT.NOT_FOUND_TOUR);
    }
  } catch (error) {
    res.status(500).send({ message: TOURCONSTANT.SYSTEM_ERROR });
  }
};

const getTourStatusFail = async (req, res) => {
  try {
    const allTour = await tour
      .find({ statusTour: false })
      .populate(["hotel", "place", "employee"]);
    if (allTour.length != 0) {
      res.status(200).send(allTour);
    } else {
      res.status(200).send(TOURCONSTANT.NOT_FOUND_TOUR);
    }
  } catch (error) {
    res.status(500).send({ message: TOURCONSTANT.SYSTEM_ERROR });
  }
};
module.exports = {
  getAllTour,
  getTourById,
  addTour,
  deleteTour,
  updateTour,

  getTourByIdUpdate,

  getTourStatusTrue,
  getTourStatusFail,
};
