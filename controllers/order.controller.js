const order = require('../models/order.model');
const ORDERCONSTANT = require('../constants/order.constant');
const TOURCONSTANT = require('../constants/tour.constant');
const seatDetail = require('../models/seatDetail.model');
const productModel = require('../models/product.model');
const tour = require('../models/tour.model');
const orderDetail = require('../models/orderDetail.model');
const { start } = require('pm2');

const getOrderDetal = async (req, res) => {
  try {
    const { orderCode } = req.body;
    let query = {};
    orderCode ? (query.orderCode = orderCode) : '';
    const listOrder = await order
      .find(query)
      .populate({
        path: 'orderDetail',
        // populate trong models con
        populate: {
          path: 'product',
          // select những thuộc tính cần thiết
          select: { productName: 1, quantity: 1, price: 1, image: 1 },
        },
      })
      .populate([
        {
          path: 'seatDetail',
          populate: [
            {
              path: 'customer',
              select: {
                fullName: 1,
                email: 1,
                phone: 1,
                address: 1,
              },
            },
            {
              path: 'tour',
              select: {
                tourName: 1,
                listImage: 1,
                startDate: 1,
                startPlace: 1,
                priceDetail: 1,
                statusOrder: 1,
              },
            },
          ],
        },
      ]);

    if (listOrder.length != 0) {
      res.status(200).send(listOrder);
    } else {
      res.status(200).send({ message: ORDERCONSTANT.NOT_FIND_ORDER });
    }
  } catch (error) {
    res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
  }
};
const getAllOrder = async (req, res) => {
  try {
    // const { orderCode } = req.body;
    // let query = {};
    // orderCode ? (query.orderCode = orderCode) : '';
    const listOrder = await order
      .find({})
      .populate({
        path: 'orderDetail',
        // populate trong models con
        populate: {
          path: 'product',
          // select những thuộc tính cần thiết
          select: { productName: 1, quantity: 1, price: 1, image: 1 },
        },
      })
      .populate([
        {
          path: 'seatDetail',
          populate: [
            {
              path: 'customer',
              select: {
                fullName: 1,
                email: 1,
                phone: 1,
                address: 1,
              },
            },
            {
              path: 'tour',
              select: {
                tourName: 1,
                listImage: 1,
                startDate: 1,
                startPlace: 1,
                priceDetail: 1,
                statusOrder: 1,
              },
            },
          ],
        },
      ]);

    if (listOrder.length != 0) {
      res.status(200).send(listOrder);
    } else {
      res.status(200).send({ message: ORDERCONSTANT.NOT_FIND_ORDER });
    }
  } catch (error) {
    res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
  }
};

const getOrderSuccess = async (req, res) => {
  try {
    var currDate = new Date();
    currDate.setDate(currDate.getDate() - 1);
    const getListOrder = await order
      .find({})
      .where('orderDate')
      .gte(currDate)
      .populate({
        path: 'orderDetail',
        // populate trong models con
        populate: {
          path: 'product',
          // select những thuộc tính cần thiết
          select: { productName: 1, quantity: 1, price: 1, image: 1 },
        },
      })
      .populate([
        {
          path: 'seatDetail',
          populate: [
            {
              path: 'customer',
              select: {
                fullName: 1,
                email: 1,
                phone: 1,
                address: 1,
              },
            },
            {
              path: 'tour',
              select: {
                tourName: 1,
                listImage: 1,
                startDate: 1,
                startPlace: 1,
                priceDetail: 1,
                statusOrder: 1,
              },
            },
          ],
        },
      ]);
    if (getListOrder.length != 0) {
      res.status(200).send(getListOrder);
    } else {
      res.status(200).send({ message: ORDERCONSTANT.NOT_FIND_ORDER });
    }
  } catch (error) {
    console.log('cancel tour Error');
    res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
  }
};

const getOrderFail = async (req, res) => {
  try {
    var currDate = new Date();
    currDate.setDate(currDate.getDate() - 1);
    const getListOrder = await order
      .find({ $or: [{ statusOrder: 'cancelled' }, { statusOrder: 'waiting' }] })
      .where('orderDate')
      .lt(currDate)
      .populate({
        path: 'orderDetail',
        // populate trong models con
        populate: {
          path: 'product',
          // select những thuộc tính cần thiết
          select: { productName: 1, quantity: 1, price: 1, image: 1 },
        },
      })
      .populate([
        {
          path: 'seatDetail',
          populate: [
            {
              path: 'customer',
              select: {
                fullName: 1,
                email: 1,
                phone: 1,
                address: 1,
              },
            },
            {
              path: 'tour',
              select: {
                tourName: 1,
                listImage: 1,
                startDate: 1,
                startPlace: 1,
                priceDetail: 1,
                statusOrder: 1,
              },
            },
          ],
        },
      ]);
    if (getListOrder.length != 0) {
      res.status(200).send(getListOrder);
    } else {
      res.status(200).send({ message: ORDERCONSTANT.NOT_FIND_ORDER });
    }
  } catch (error) {
    console.log('cancel tour Error');
    res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
  }
};

const cancelOrder = async (req, res) => {
  try {
    const id = req.params.id;
    const getOrderById = await order
      .findById({ _id: id })
      .populate(['orderDetail', 'seatDetail']);
    const listOrderDetail = getOrderById.orderDetail;
    const getSeatDetail = getOrderById.seatDetail;
    const getTour = await tour.findOne({ _id: getSeatDetail.tour });
    getTour.seatStatus = 'Còn Chỗ';
    getTour.numberTicket =
      getTour.numberTicket + getSeatDetail.listCutomerTour.length;
    tour
      .findByIdAndUpdate(
        { _id: getTour._id },
        {
          $set: {
            numberTicket: getTour.numberTicket,
            seatStatus: getTour.seatStatus,
            statusTour : true
          },
        }
      )
      .then((value) => {
        console.log('Update tour thành công');
      });
    if (listOrderDetail.length != 0) {
      for (let i = 0; i < listOrderDetail.length; i++) {
        const element = listOrderDetail[i];
        var getProductById = await productModel.findById({
          _id: element.product,
        });
        getProductById.quantity = getProductById.quantity + element.quantity;
        productModel
          .findByIdAndUpdate(
            { _id: getProductById._id },
            { $set: { quantity: getProductById.quantity } }
          )
          .then((value) => {
            console.log('Cộng số lượng product thành công');
          });
      }
    }
    order
      .findByIdAndUpdate({ _id: id }, { $set: { statusOrder: 'cancelled' } })
      .then((value) => {
        res.status(200).send(value);
      })
      .catch((error) => {
        res.status(500).send({ message: ORDERCONSTANT.CHECK_ORDER_FAIL });
      });
  } catch (error) {
    res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
  }
};

const checkOrder = async (req, res) => {
  try {
    const id = req.params.id;
    order
      .findByIdAndUpdate({ _id: id }, { $set: { statusOrder: 'checked' } })
      .then((value) => {
        res.status(200).send(value);
      })
      .catch((error) => {
        res.status(500).send({ message: ORDERCONSTANT.CHECK_ORDER_FAIL });
      });
  } catch (error) {
    res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
  }
};

const getTourByOrder = async (req, res) => {
  try {
    var getTours = [];
    const getOrders = await order
      .find({ $or: [{ statusOrder: 'checked' }, { statusOrder: 'waiting' }] })
      .populate([
        {
          path: 'seatDetail',
          populate: [
            {
              path: 'customer',
              select: {
                fullName: 1,
                email: 1,
                phone: 1,
                address: 1,
              },
            },
            {
              path: 'tour',
              select: {
                tourId: 1,
                tourName: 1,
                listImage: 1,
                startDate: 1,
                startPlace: 1,
                priceDetail: 1,
                statusOrder: 1,
              },
            },
          ],
        },
      ]);
    if (getOrders.length != 0) {
      getTours.push(getOrders[0].seatDetail.tour);
      for (let x = 1; x < getOrders.length; x++) {
        var kiemTraTour = true;
        for (let y = 0; y < getTours.length; y++) {
          console.log(getOrders[x].seatDetail.tour);
          if (getTours[y]._id == getOrders[x].seatDetail.tour._id) {
            kiemTraTour = false;
          }
          if (kiemTraTour == false) {
            break;
          }
          if (getTours.length - 1 == y) {
            getTours.push(getOrders[x].seatDetail.tour);
            break;
          }
        }
      }
      res.status(200).send(getTours);
    } else {
      res.status(200).send({ message: TOURCONSTANT.NOT_FOUND_TOUR });
    }
  } catch (error) {
    res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
  }
};

const getListCustomerTour = async (req, res) => {
  try {
    const tour = req.body.id;
    var getSeatDetailsBytour = [];
    const getOrders = await order
      .find({ $or: [{ statusOrder: 'checked' }, { statusOrder: 'waiting' }] })
      .populate(['seatDetail', 'customer']);

    if (getOrders.length != 0) {
      for (let index = 0; index < getOrders.length; index++) {
        const element = getOrders[index].seatDetail;
        if (element.tour == tour) {
          getSeatDetailsBytour.push(element);
        }
      }
    }
    // const getSeatDetailsBytour = await seatDetail.find({ tour: tour });
    if (getSeatDetailsBytour.length != 0) {
      res.status(200).send(getSeatDetailsBytour);
    } else {
      res.status(200).send({ message: ORDERCONSTANT.NOT_FIND_ORDER });
    }
  } catch (error) {
    res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
  }
};

module.exports = {
  getAllOrder,
  getOrderDetal,
  getOrderSuccess,
  getOrderFail,
  cancelOrder,
  checkOrder,
  getTourByOrder,
  getListCustomerTour
};
