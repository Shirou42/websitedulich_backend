const place = require('../models/place.model');
const PLACECONSTANT = require('../constants/place.constant');

const getAllPlace = async (req, res) => {
  try {
    const allPlace = await place.find({});
    if (allPlace !== null) {
      res.status(200).send(allPlace);
    } else {
      res.status(200).send({ message: PLACECONSTANT.NOT_FOUND_PLACE });
    }
  } catch (error) {
    res.status(500).send({ message: PLACECONSTANT.SYSTEM_ERROR });
  }
};

const getPlaceById = async (req, res) => {
  try {
    const id = req.body.id;
    const placeById = await place.findById({ _id: id });
    if (placeById.length != 0) {
      res.status(200).send(placeById);
    } else {
      res.status(200).send({ message: PLACECONSTANT.NOT_FOUND_PLACE });
    }
  } catch (error) {
    res.status(500).send({ message: PLACECONSTANT.SYSTEM_ERROR });
  }
};

const addPlace = async (req, res) => {
  try {
    var newPlace = new place({
      placeName: req.body.placeName,
      type: req.body.type,
      city: req.body.city,
      district: req.body.district,
      apartmentNumber: req.body.apartmentNumber,
    });
    newPlace.save(function (err) {
      if (err) {
        res.status(500).send({ message: PLACECONSTANT.ADD_PLACE_FAIL });
      } else {
        res.status(200).send(newPlace);
      }
    });
  } catch (error) {
    res.status(500).send({ message: PLACECONSTANT.SYSTEM_ERROR });
  }
};

const deletePlace = async (req, res) => {
  try {
    const id = req.params.id;
    place.findByIdAndDelete({ _id: id }, function (err) {
      if (err) {
        res.status(500).send({ message: PLACECONSTANT.DELETE_PLACE_FAIL });
      } else {
        res.status(200).send({ message: PLACECONSTANT.DELETE_PLACE_SUCCESS });
      }
    });
  } catch (error) {
    res.status(500).send({ message: PLACECONSTANT.SYSTEM_ERROR });
  }
};

const updatePlace = async (req, res) => {
  try {
    const id = req.params.id;
    const { placeName, type, city, district, apartmentNumber } = req.body;
    let query = {};
    placeName ? (query.placeName = placeName) : '';
    type ? (query.type = type) : '';
    city ? (query.city = city) : '';
    district ? (query.district = district) : '';
    apartmentNumber ? (query.apartmentNumber = apartmentNumber) : '';
    place.findByIdAndUpdate({ _id: id }, { $set: query }, function (err) {
      if (err) {
        res.status(500).send({ message: PLACECONSTANT.UPDATE_PLACE_FAIL });
      } else {
        res.status(200).send({ message: PLACECONSTANT.UPDATE_PLACE_SUCCESS });
      }
    });
  } catch (error) {
    res.status(500).send({ message: PLACECONSTANT.SYSTEM_ERROR });
  }
};

module.exports = {
  getAllPlace,
  getPlaceById,
  addPlace,
  deletePlace,
  updatePlace,
};
