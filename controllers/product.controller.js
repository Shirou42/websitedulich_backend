const product = require('../models/product.model');
const PRODUCTCONSTANT = require('../constants/product.constant');
const imageDBS3 = require('../config/imageDBS3');

const getAllProduct = async (req, res) => {
  try {
    const allProduct = await product.find({});
    if (allProduct.length !== 0) {
      res.status(200).send(allProduct);
    } else {
      res.status(200).send({ message: PRODUCTCONSTANT.NOT_FOUND_PRODUCT });
    }
  } catch (error) {
    res.status(500).send({ message: PRODUCTCONSTANT.SYSTEM_ERROR });
  }
};
const getProductById = async (req, res) => {
  try {
    const id = req.body.id;
    console.log(id);
    const productById = await product.findById({ _id: id });
    if (productById.length != 0) {
      res.status(200).send(productById);
    } else {
      res.status(200).send({ message: PRODUCTCONSTANT.NOT_FOUND_PRODUCT });
    }
  } catch (error) {
    res.status(500).send({ message: PRODUCTCONSTANT.SYSTEM_ERROR });
  }
};

const addProduct = async (req, res) => {
  try {
    const productCode = req.body.productCode;
    const productExist = await product.findOne({ productCode: productCode });
    let files = req.files;
    if (files != null) {
      let image = await files.files;
      const uploadS3 = await imageDBS3.uploadImages(image);
      if (productExist == null) {
        const newProduct = new product({
          productCode: productCode,
          productName: req.body.productName,
          image: uploadS3,
          price: req.body.price,
          quantity: req.body.quantity,
          typesOftourism: req.body.typesOftourism,
          description: req.body.description,
        });
        console.log(newProduct);
        newProduct
          .save()
          .then((value) => {
            res.status(200).send(value);
          })
          .catch((error) => {
            res.status(500).send({ message: PRODUCTCONSTANT.ADD_PRODUCT_FAIL });
          });
      } else {
        res.status(200).send({ message: PRODUCTCONSTANT.PRODUCT_EXIST });
      }
    } else {
      res.status(200).send({ message: PRODUCTCONSTANT.PRODUCT_IMAGE_FAIL });
    }
  } catch (error) {
    res.status(500).send({ message: PRODUCTCONSTANT.SYSTEM_ERROR });
  }
};

const deleteProduct = async (req, res) => {
  try {
    const id = req.params.id;
    product.findByIdAndDelete({ _id: id }, function (err) {
      if (err) {
        res.status(500).send({ message: PRODUCTCONSTANT.DELETE_PRODUCT_FAIL });
      } else {
        res
          .status(200)
          .send({ message: PRODUCTCONSTANT.DELETE_PRODUCT_SUCCESS });
      }
    });
  } catch (error) {
    res.status(500).send({ message: PRODUCTCONSTANT.SYSTEM_ERROR });
  }
};

const updateProduct = async (req, res) => {
  try {
    const id = req.params.id;
    const productExist = await product.findOne({ _id: id });
    if (productExist != null) {
      const {
        productName,
        price,
        quantity,
        typesOftourism,
        description,
      } = req.body;
      let query = {};
      productName ? (query.productName = productName) : '';
      price ? (query.price = price) : '';
      quantity ? (query.quantity = quantity) : '';
      typesOftourism ? (query.typesOftourism = typesOftourism) : '';
      description ? (query.description = description) : '';
      let files = req.files;
      if (files != null) {
        let image = await files.image;
        const uploadS3 = await imageDBS3.uploadImages(image);
        query.image = uploadS3;
      }
      product.findByIdAndUpdate({ _id: id }, { $set: query }, function (err) {
        if (err) {
          res
            .status(500)
            .send({ message: PRODUCTCONSTANT.UPDATE_PRODUCT_FAIL });
        } else {
          res
            .status(200)
            .send({ message: PRODUCTCONSTANT.UPDATE_PRODUCT_SUCCESS });
        }
      });
    } else {
      res.status(500).send({ message: PRODUCTCONSTANT.PRODUCT_EXIST });
    }
  } catch (error) {
    res.status(500).send({ message: PRODUCTCONSTANT.SYSTEM_ERROR });
  }
};

module.exports = {
  getAllProduct,
  getProductById,
  addProduct,
  deleteProduct,
  updateProduct,
};
