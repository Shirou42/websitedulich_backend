const order = require('../models/order.model');
const ORDERCONSTANT = require('../constants/order.constant');
const TOURCONSTANT = require('../constants/tour.constant');
const seatDetail = require('../models/seatDetail.model');
const productModel = require('../models/product.model');
const tour = require('../models/tour.model');
const orderDetail = require('../models/orderDetail.model');
const { start } = require('pm2');

// thống kế từng tháng qua các năm
const getOrderByMonth = async (req, res) => {
    try {
        var startDate = new Date(req.body.startDate);
        var endDate = new Date(req.body.endDate);
        var monthDate = new Date(req.body.startDate);
        var startYear = startDate.getFullYear();
        var endYear = endDate.getFullYear();
        var startMonth = startDate.getMonth();
        var endMonth = endDate.getMonth();
        var statistic = [];
        var getOrder = [];
        monthDate.setMonth(monthDate.getMonth() + 1)
        monthDate.setDate(1);
        if (startYear < endYear) {
            if (startYear < endYear) {
                console.log('start year < end year');
                for (let index = startMonth + 1; index < 12; index++) {
                    console.log('MonthDate' + monthDate.getMonth());
                    console.log('StartDate' + startDate.getMonth());
                    console.log(startDate);
                    console.log(monthDate);
                    getOrder = await order.find({}).where('orderDate').gte(startDate).lt(monthDate).count();
                    statistic.push(getOrder.length);
                    monthDate.setMonth(monthDate.getMonth() + 1);
                    startDate.setMonth(startDate.getMonth() + 1);
                    startDate.setDate(1);
                }
                console.log('MonthDate' + monthDate.getMonth());
                console.log('StartDate' + startDate.getMonth());
                console.log(startDate);
                console.log(monthDate);
                getOrder = await order.find({}).where('orderDate').gte(startDate).lt(monthDate).count();
                statistic.push(getOrder.length);
                startYear++;
            }
            while (startYear < endYear) {
                console.log('while start year < end year');
                monthDate.setMonth(monthDate.getMonth() + 1);
                startDate.setMonth(startDate.getMonth() + 1);
                for (let index = 1; index < 12; index++) {
                    console.log('MonthDate' + monthDate.getMonth());
                    console.log('StartDate' + startDate.getMonth());
                    console.log(startDate);
                    console.log(monthDate);
                    getOrder = await order.find({}).where('orderDate').gte(startDate).lt(monthDate).count();
                    statistic.push(getOrder.length);
                    monthDate.setMonth(monthDate.getMonth() + 1);
                    startDate.setMonth(startDate.getMonth() + 1);
                }
                console.log('MonthDate' + monthDate.getMonth());
                console.log('StartDate' + startDate.getMonth());
                console.log(startDate);
                console.log(monthDate);
                getOrder = await order.find({}).where('orderDate').gte(startDate).lt(monthDate).count();
                statistic.push(getOrder.length);
                startYear++;
            }
            if (startYear = endYear) {
                console.log('start year = end year');
                monthDate.setMonth(monthDate.getMonth() + 1);
                startDate.setMonth(startDate.getMonth() + 1);
                for (let index = 0; index < endMonth; index++) {
                    console.log('MonthDate' + monthDate.getMonth());
                    console.log('StartDate' + startDate.getMonth());
                    console.log(startDate);
                    console.log(monthDate);
                    getOrder = await order.find({}).where('orderDate').gte(startDate).lt(monthDate).count();
                    statistic.push(getOrder.length);
                    monthDate.setMonth(monthDate.getMonth() + 1);
                    startDate.setMonth(startDate.getMonth() + 1);
                }
                console.log('StartDate' + startDate.getMonth());
                console.log('EndDate' + endDate.getMonth());
                console.log(startDate);
                console.log(endDate);
                getOrder = await order.find({}).where('orderDate').gte(startDate).lte(endDate).count();
                statistic.push(getOrder.length);
            }
        }
        else {
            console.log('start year = end year v2');
            for (let index = startMonth; index < endMonth; index++) {
                getOrder = await order.find({}).where('orderDate').gte(startDate).lt(monthDate).count();
                statistic.push(getOrder.length);
                monthDate.setMonth(monthDate.getMonth() + 1);
                startDate.setMonth(startDate.getMonth() + 1);
                startDate.setDate(1);
            }
            getOrder = await order.find({}).where('orderDate').gte(startDate).lte(endDate).count();
            statistic.push(getOrder);
        }
        console.log(statistic.length);
        res.status(200).send({ statistic });
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
    }
};

// thống kế qua các năm
const getOrderByYear = async (req, res) => {
    try {
        var startYear = req.body.startYear;
        var endYear = req.body.endYear;
        const statusOrder = req.body.statusOrder;
        var query = {};
        statusOrder ? (query.statusOrder = statusOrder) : '';
        var statistic = [];
        var getOrder = [];
        var startDate = new Date('2000-1-2');
        startDate.setFullYear(startYear);
        startDate.setUTCHours(0, 0, 0, 0);
        var endDate = new Date('2000-1-1');
        endDate.setUTCHours(0, 0, 0, 0)
        endDate.setFullYear(startYear);
        for (let index = startYear; index <= endYear; index++) {
            getOrder = await order.find(query)
                .where('orderDate').gte(startDate).lte(endDate)
                .count();
            statistic.push(getOrder);
            startDate.setUTCFullYear(startDate.getFullYear() + 1);
            endDate.setUTCFullYear(endDate.getFullYear() + 1);
        }
        console.log(statistic.length);
        res.status(200).send({ statistic });
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
    }
};

const getOrderByMonthInYear = async (req, res) => {
    try {
        const year = req.body.year;
        const statusOrder = req.body.statusOrder;
        var query = {};
        statusOrder ? (query.statusOrder = statusOrder) : '';
        var statistic = [];
        var getOrder = [];
        var startDate = new Date('2000-1-2');
        startDate.setFullYear(year);
        startDate.setUTCHours(0, 0, 0, 0);
        var endDate = new Date('2000-2-2');
        endDate.setUTCHours(0, 0, 0, 0)
        endDate.setFullYear(year);
        for (let index = 1; index < 13; index++) {
            getOrder = await order.find(query)
                .where('orderDate').gte(startDate).lt(endDate)
                .count();
            statistic.push(getOrder);
            startDate.setMonth(startDate.getMonth() + 1);
            endDate.setMonth(endDate.getMonth() + 1);
        }
        console.log(statistic.length);
        res.status(200).send({ statistic });
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
    }
};

const getNumberProductInMonth = async (req, res) => {
    try {
        const year = req.body.year;
        const month = req.body.month;

        var startDate = new Date('2000-1-2');
        startDate.setUTCHours(0, 0, 0, 0);
        startDate.setMonth(month - 1)
        startDate.setFullYear(year);

        var endDate = new Date('2000-1-2');
        endDate.setUTCHours(0, 0, 0, 0)
        endDate.setMonth(month)
        endDate.setFullYear(year);

        var infoProduct = {};
        var analyProduct = [];
        var indexProduct = 0;
        // const getOrder = await order.aggregate()
        // .match({$and:[{orderDate:{ $lt: endDate }},{orderDate:{$gte:startDate}}] })
        const getOrder = await order.find({})
            .where('orderDate').gte(startDate).lt(endDate)
            .populate({
                path: 'orderDetail',
                // populate trong models con
                populate: {
                    path: 'product',
                    // select những thuộc tính cần thiết
                    select: { productName: 1, quantity: 1, price: 1, image: 1 },
                },
            })
            .select('orderDetail')
        if (getOrder.length != 0) {
            for (let x = 0; x < getOrder.length; x++) {
                const element = getOrder[x].orderDetail;
                for (let y = 0; y < element.length; y++) {
                    infoProduct = {
                        id: element[y].product._id,
                        productName: element[y].product.productName,
                        quantity: element[y].quantity
                    }
                    indexProduct = analyProduct.findIndex((pro) => pro.id == infoProduct.id);
                    if (indexProduct >= 0) {
                        analyProduct[indexProduct].quantity = analyProduct[indexProduct].quantity + infoProduct.quantity;
                    }
                    else {
                        analyProduct.push(infoProduct)
                    }
                }
            }
            analyProduct.sort(function (a, b) {
                return b.quantity - a.quantity;
            });
            res.status(200).send({ analyProduct: analyProduct.slice(0, 5) });
        }
        else {
            res.status(500).send({ message: 'Không có sản phẩm' });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
    }
};

const getNumberTourInMonth = async (req, res) => {
    try {
        const year = req.body.year;
        const month = req.body.month;

        var startDate = new Date('2000-1-2');
        startDate.setUTCHours(0, 0, 0, 0);
        startDate.setMonth(month - 1)
        startDate.setFullYear(year);

        var endDate = new Date('2000-1-2');
        endDate.setUTCHours(0, 0, 0, 0)
        endDate.setMonth(month)
        endDate.setFullYear(year);

        // var listProduct = [];
        var infoTour = {};
        var analyTour = [];
        var indexTour = 0;
        // const getOrder = await order.aggregate()
        // .match({$and:[{orderDate:{ $lt: endDate }},{orderDate:{$gte:startDate}}] })
        const getOrder = await order.find({})
            .where('orderDate').gte(startDate).lt(endDate)
            .populate([
                {
                    path: 'seatDetail',
                    populate: [
                        {
                            path: 'customer',
                            select: {
                                fullName: 1,
                                email: 1,
                                phone: 1,
                                address: 1,
                            },
                        },
                        {
                            path: 'tour',
                            select: {
                                tourName: 1,
                                listImage: 1,
                                startDate: 1,
                                startPlace: 1,
                                priceDetail: 1,
                                statusOrder: 1,
                            },
                        },
                    ],
                },
            ]);
        if (getOrder.length != 0) {
            for (let index = 0; index < getOrder.length; index++) {
                const element = getOrder[index].seatDetail;
                infoTour = {
                    tourName: element.tour.tourName.split(/[:()]+/)[1],
                    quantity: element.listCutomerTour.length
                }
                indexTour = analyTour.findIndex((pro) => pro.tourName == infoTour.tourName);
                if (indexTour >= 0) {
                    analyTour[indexTour].quantity = analyTour[indexTour].quantity + infoTour.quantity;
                }
                else {
                    analyTour.push(infoTour)
                }
            }
            analyTour.sort(function (a, b) {
                return b.quantity - a.quantity;
            });
            res.status(200).send({ analyTour: analyTour.slice(0, 5) });
        }
        else {
            res.status(500).send({ message: 'Không có hoá đơn' });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
    }
};

const getRevenueInQuarter = async (req, res) => {
    try {
        const year = req.body.year;

        var startDate = new Date('2000-1-2');
        startDate.setUTCHours(0, 0, 0, 0);
        startDate.setFullYear(year);
        var endDate = new Date('2000-4-2');
        endDate.setUTCHours(0, 0, 0, 0)
        endDate.setFullYear(year);

        var revenue = {};
        var analyRevenue = [];

        var revenueProduct = 0;
        var revenueCustomer = 0;
        var revenueTotal = 0;
        for (let x = 0; x < 4; x++) {
            revenueProduct = 0;
            revenueCustomer = 0;
            revenueTotal = 0;
            const getOrder = await order.find({statusOrder:'checked'})
            .where('orderDate').gte(startDate).lt(endDate)
            .populate(["orderDetail","seatDetail"])

            if (getOrder.length != 0) {
                for (let index = 0; index < getOrder.length; index++) {
                    revenueCustomer = revenueCustomer + getOrder[index].seatDetail.totalPrice;
                    getOrder[index].orderDetail.forEach(element => revenueProduct = revenueProduct + element.totalPrice);
                    revenueTotal = revenueTotal + getOrder[index].total;
                }
            }
            revenue = {
                revenueCustomer: revenueCustomer,
                revenueProduct: revenueProduct,
                revenueTotal: revenueTotal
            }
            analyRevenue.push(revenue);
            startDate.setMonth(startDate.getMonth() + 3);
            endDate.setMonth(endDate.getMonth() + 3);
        }
        res.status(200).send(analyRevenue)
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
    }
};

const getRevenueInMonth = async (req, res) => {
    try {
        const year = req.body.year;

        var startDate = new Date('2000-1-2');
        startDate.setUTCHours(0, 0, 0, 0);
        startDate.setFullYear(year);
        var endDate = new Date('2000-2-2');
        endDate.setUTCHours(0, 0, 0, 0)
        endDate.setFullYear(year);

        var revenue = {};
        var analyRevenue = [];

        var revenueProduct = 0;
        var revenueCustomer = 0;
        var revenueTotal = 0;
        for (let x = 1; x < 13; x++) {
            revenueProduct = 0;
            revenueCustomer = 0;
            revenueTotal = 0;
            const getOrder = await order.find({statusOrder:'checked'})
            .where('orderDate').gte(startDate).lt(endDate)
            .populate(["orderDetail","seatDetail"])

            if (getOrder.length != 0) {
                for (let index = 0; index < getOrder.length; index++) {
                    revenueCustomer = revenueCustomer + getOrder[index].seatDetail.totalPrice;
                    getOrder[index].orderDetail.forEach(element => revenueProduct = revenueProduct + element.totalPrice);
                    revenueTotal = revenueTotal + getOrder[index].total;
                }
            }
            revenue = {
                revenueCustomer: revenueCustomer,
                revenueProduct: revenueProduct,
                revenueTotal: revenueTotal
            }
            analyRevenue.push(revenue);
            startDate.setMonth(startDate.getMonth() + 1);
            endDate.setMonth(endDate.getMonth() + 1);
        }
        res.status(200).send(analyRevenue)
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: ORDERCONSTANT.SYSTEM_ERROR });
    }
};
module.exports = {
    getOrderByMonth,
    getOrderByYear,
    getOrderByMonthInYear,
    getNumberProductInMonth,
    getNumberTourInMonth,
    getRevenueInMonth,
    getRevenueInQuarter
};
