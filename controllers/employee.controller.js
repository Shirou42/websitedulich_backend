const employee = require('../models/employee.model');
const EMPLOYEECONSTANT = require('../constants/employee.constant');
const imageDBS3 = require('../config/imageDBS3');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
const jwt_key = `${process.env.jwt_key}`;

const getAllEmployee = async (req, res) => {
  try {
    const allEmployee = await employee.find({});
    if (allEmployee.length !== 0) {
      res.status(200).send(allEmployee);
    } else {
      res.status(200).send({ message: EMPLOYEECONSTANT.NOT_FOUND_EMPLOYEE });
    }
  } catch (error) {
    res.status(500).send({ message: EMPLOYEECONSTANT.SYSTEM_ERROR });
  }
};

const getEmployeeById = async (req, res) => {
  try {
    const id = req.body.id;
    const employeeById = await employee.findById({ _id: id });
    if (employeeById.length != 0) {
      res.status(200).send(employeeById);
    } else {
      res.status(200).send({ message: EMPLOYEECONSTANT.NOT_FOUND_EMPLOYEE });
    }
  } catch (error) {
    res.status(500).send({ message: EMPLOYEECONSTANT.SYSTEM_ERROR });
  }
};

const addEmployee = async (req, res) => {
  try {
    const { employeeId, fullName, gender, dateOfbirth, email, role } = req.body;
    const password = bcrypt.hashSync('121212', 8);

    let files = req.files;
    let query = {
      employeeId: employeeId,
      fullName: fullName,
      gender: gender,
      dateOfbirth: dateOfbirth,
      email: email,
      role: role,
      password: password,
    };
    if (files != null) {
      let image = await files.files;
      const uploadS3 = await imageDBS3.uploadImages(image);
      query.image = uploadS3;
    }
    const newEmployee = new employee(query);
    newEmployee.save(function (err) {
      if (err) {
        res.status(200).send({ message: EMPLOYEECONSTANT.ADD_EMPLOYEE_FAIL });
      } else {
        res
          .status(200)
          .send({ message: EMPLOYEECONSTANT.ADD_EMPLOYEE_SUCCESS });
      }
    });
  } catch (error) {
    res.status(500).send({ message: EMPLOYEECONSTANT.SYSTEM_ERROR });
  }
};

const deleteEmployee = async (req, res) => {
  try {
    const id = req.params.id;
    employee.findByIdAndDelete({ _id: id }, function (err) {
      if (err) {
        res
          .status(200)
          .send({ message: EMPLOYEECONSTANT.DELETE_EMPLOYEE_FAIL });
      } else {
        res
          .status(200)
          .send({ message: EMPLOYEECONSTANT.DELETE_EMPLOYEE_SUCCESS });
      }
    });
  } catch (error) {
    res.status(500).send({ message: EMPLOYEECONSTANT.SYSTEM_ERROR });
  }
};

const updateEmployee = async (req, res) => {
  try {
    const id = req.params.id;
    const { employeeId, fullName, gender, dateOfbirth, email, role } = req.body;
    let files = req.files;
    let query = {};
    if (files != null) {
      let image = await files.file;
      const uploadS3 = await imageDBS3.uploadImages(image);
      query.image = uploadS3;
    }
    employeeId ? (query.employeeId = employeeId) : '';
    fullName ? (query.fullName = fullName) : '';
    gender ? (query.gender = gender) : '';
    dateOfbirth ? (query.dateOfbirth = dateOfbirth) : '';
    email ? (query.email = email) : '';
    role ? (query.role = role) : '';
    employee.findByIdAndUpdate({ _id: id }, { $set: query }, function (err) {
      if (err) {
        res
          .status(200)
          .send({ message: EMPLOYEECONSTANT.UPDATE_EMPLOYEE_FAIL });
      } else {
        res
          .status(200)
          .send({ message: EMPLOYEECONSTANT.UPDATE_EMPLOYEE_SUCCESS });
      }
    });
  } catch (error) {
    res.status(500).send({ message: EMPLOYEECONSTANT.SYSTEM_ERROR });
  }
};

module.exports = {
  getAllEmployee,
  getEmployeeById,
  addEmployee,
  deleteEmployee,
  updateEmployee,
};
