const CUSTOMERCONSTANT = require('../constants/customer.constant');
const customer = require('../models/customer.model');
const order = require('../models/order.model');
const ORDERCONSTANT = require('../constants/order.constant');
const TOURCONSTANT = require('../constants/tour.constant');
const seatDetail = require('../models/seatDetail.model');
const productModel = require('../models/product.model');
const tour = require('../models/tour.model');
const orderDetail = require('../models/orderDetail.model');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
const jwt_key = `${process.env.jwt_key}`;
require('dotenv').config();

const getAllCustomer = async (req, res) => {
  try {
    const allCustomer = await customer.find({});
    if (allCustomer.length != 0) {
      res.status(200).send(allCustomer);
    } else {
      res.status(200).send({ message: CUSTOMERCONSTANT.NOT_FOUND_CUSTOMER });
    }
  } catch (error) {
    res.status(500).send({ message: CUSTOMERCONSTANT.SYSTEM_ERROR });
  }
};

const getCustomerById = async (req, res) => {
  try {
    const id = req.body.id;
    const customerById = await customer.findById({ _id: id });
    if (customerById.length != 0) {
      res.status(200).send(customerById);
    } else {
      res.status(200).send({ message: CUSTOMERCONSTANT.NOT_FOUND_CUSTOMER });
    }
  } catch (error) {
    res.status(500).send({ message: CUSTOMERCONSTANT.SYSTEM_ERROR });
  }
};

const addCustomer = async (req, res) => {
  try {
    const { phone, email } = req.body;
    // Mã hóa password
    const password = bcrypt.hashSync(req.body.password, 8);
    // Xem khách hàng tồn tại hay chưa qua di động hoặc email
    const customerExist = await customer.findOne({
      $or: [{ phone: phone }, { email: email }],
    });
    if (customerExist == null) {
      const newCustomer = new customer({
        fullName: req.body.fullName,
        gender: req.body.gender,
        birthday: req.body.birthday,
        phone: phone,
        email: email,
        identityCard: req.body.identityCard,
        address: req.body.address,
        password: password,
      });
      newCustomer.token = jwt.sign({ _id: newCustomer._id }, jwt_key);
      newCustomer
        .save()
        .then((value) => {
          res.status(200).send(value);
        })
        .catch((error) => {
          res.status(500).send({ message: CUSTOMERCONSTANT.ADD_CUSTOMER_FAIL });
        });
    } else {
      res.status(500).send({ message: CUSTOMERCONSTANT.CUSTOMER_EXISTED });
    }
  } catch (error) {
    res.status(500).send({ message: CUSTOMERCONSTANT.SYSTEM_ERROR });
  }
};

const deleteCustomer = async (req, res) => {
  try {
    const id = req.params.id;
    customer
      .findByIdAndDelete({ _id: id })
      .then((value) => {
        res.status(200).send(value);
      })
      .catch((error) => {
        res
          .status(500)
          .send({ message: CUSTOMERCONSTANT.DELETE_CUSTOMER_FAIL });
      });
  } catch (error) {
    res.status(500).send({ message: CUSTOMERCONSTANT.SYSTEM_ERROR });
  }
};

const updateCustomer = async (req, res) => {
  try {
    const id = req.params.id;
    const {
      fullName,
      gender,
      dateOfbirth,
      phone,
      email,
      identityCard,
      address,
    } = req.body;
    let query = {};
    fullName ? (query.fullName = fullName) : '';
    gender ? (query.gender = gender) : '';
    dateOfbirth ? (query.dateOfbirth = dateOfbirth) : '';
    if (phone !== undefined) {
      const customerPhone = await customer.findOne({ phone: phone });
      if (customerPhone === null) {
        query.phone = phone;
      }
    }
    if (email !== undefined) {
      const customerEmail = await customer.findOne({ email: email });
      if (customerEmail === null) {
        email ? (query.email = email) : '';
      }
    }
    identityCard ? (query.identityCard = identityCard) : '';
    address ? (query.address = address) : '';
    customer.findByIdAndUpdate({ _id: id }, { $set: query }).then((docs) => {
      res.status(200).send(docs);
    });
  } catch (error) {
    res.status(500).send({ message: CUSTOMERCONSTANT.SYSTEM_ERROR });
  }
};

const getNumberOrder = async (req, res) => {
  try {
    const customerId = req.body.customerId;
    const getOrder = await order.find({})
    var numCancelled = 0 ;
    var numWaiting = 0 ;
    var numChecked = 0;
    if(getOrder.length != 0){
      for (let index = 0; index < getOrder.length; index++) {
        const element = getOrder[index].seatDetail;
        if(element.customer = customerId){
          if(getOrder[index].statusOrder = 'cancelled'){
            numCancelled = numCancelled + 1;
          }
          else if(getOrder[index].statusOrder = 'checked'){
            numChecked = numChecked + 1;
          }
          else{
            numWaiting = numWaiting + 1;
          }
        }
      }
      res.status(200).send({ numCancelled,numChecked,numWaiting })
    }
    else{
      res.status(200).send({ message: CUSTOMERCONSTANT.CUSTOMER_NOT_ORDER })
    }
  } catch (error) {
    res.status(500).send({ message: CUSTOMERCONSTANT.SYSTEM_ERROR });
  }
};
module.exports = {
  getAllCustomer,
  getCustomerById,
  addCustomer,
  deleteCustomer,
  updateCustomer,
  getNumberOrder
};
