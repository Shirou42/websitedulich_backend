const hotel = require('../models/hotel.model');
const tour = require('../models/tour.model')
const HOTELCONSTANT = require('../constants/hotel.constant');

const getAllHotel = async (req, res) => {
  try {
    const allHotel = await hotel.find({});
    if (allHotel.length != 0) {
      res.status(200).send(allHotel);
    } else {
      res.status(200).send({ message: HOTELCONSTANT.NOT_FOUND_HOTEL });
    }
  } catch (error) {
    res.status(500).send({ message: HOTELCONSTANT.SYSTEM_ERROR });
  }
};
const getHotelById = async (req, res) => {
  try {
    const id = req.body.id;
    const hotelById = await hotel.findById({ _id: id });
    if (hotelById.length != 0) res.status(200).send(hotelById);
    else res.status(200).send({ message: HOTELCONSTANT.NOT_FOUND_HOTEL });
  } catch (error) {
    res.status(500).send({ message: HOTELCONSTANT.SYSTEM_ERROR });
  }
};
const addHotel = async (req, res) => {
  try {
    const newHotel = new hotel({
      hotelName: req.body.hotelName,
      email: req.body.email,
      phone: req.body.phone,
      city: req.body.city,
      quality: req.body.quality,
    });
    newHotel.save(function (err) {
      if (err) {
        res.status(500).send({ message: HOTELCONSTANT.ADD_HOTEL_FAIL });
      } else {
        res.status(200).send(newHotel);
      }
    });
  } catch (error) {
    res.status(500).send({ message: HOTELCONSTANT.SYSTEM_ERROR });
  }
};

const deleteHotel = async (req, res) => {
  const id = req.params.id;
  try {
    // const getTour = await tour.find({}).where('hotel').in(id);
    hotel.findByIdAndDelete({ _id: id }, function (err) {
      if (err) {
        res.status(500).send({ message: HOTELCONSTANT.DELETE_HOTEL_FAIL });
      } else {
        res.status(200).send({ message: HOTELCONSTANT.DELETE_HOTEL_SUCCESS });
      }
    });
  } catch (error) {
    res.status(500).send({ message: HOTELCONSTANT.SYSTEM_ERROR });
  }
};

const updateHotel = async (req, res) => {
  const id = req.params.id;
  const { hotelName, city, email, phone, quality } = req.body;
  let query = {};
  if (hotelName !== undefined) {
    const hotelByName = await hotel.findOne({ hotelName: hotelName });
    if (hotelByName === null) {
      query.hotelName = hotelName;
    }
  }
  city ? (query.city = city) : '';
  if (email !== undefined) {
    const hotelByEmail = await hotel.findOne({ email: email });
    if (hotelByEmail === null) {
      query.email = email;
    }
  }
  if (phone !== undefined) {
    const hotelByPhone = await hotel.findOne({ phone: phone });
    if (hotelByPhone === null) {
      query.phone = phone;
    }
  }
  quality ? (query.quality = quality) : '';
  try {
    hotel.findByIdAndUpdate({ _id: id }, { $set: query }, function (err) {
      if (err) {
        res.status(500).send({ message: HOTELCONSTANT.UPDATE_HOTEL_FAIL });
      } else {
        res.status(200).send({ message: HOTELCONSTANT.UPDATE_HOTEL_SUCCESS });
      }
    });
  } catch (error) {
    res.status(500).send({ message: HOTELCONSTANT.SYSTEM_ERROR });
  }
};

module.exports = {
  getAllHotel,
  getHotelById,
  addHotel,
  deleteHotel,
  updateHotel,
};
